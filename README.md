# PuMA Experiments

This repo contains experiments conducted using [PuMA](https://github.com/nasa/puma), which is a 3D visualizer and analyzer of porous microstructures, which can be uploaded to the program as a series of image file such as TIF, which we used in these experiments. The goal of these experiments was to upload a [CoST](https://bitbucket.org/surflab/cost/src/master/) to the visualizer and conduct stress analysis on it. The goal has not been achieved as of yet; however, we can now view a CoST in PuMA.

# Files And Essential Information

There is an inflated CoST, a TIF file of that CoST, and the PuMA tutorial code, modified to include uploading the TIF file in this repo. 


## In order to get the CoST repo to generate an inflated mesh, you will have to...

• Follow the instructions to add PyMesh and pybind11 in the CoST repo 

• Uncomment the code in inflateCoST.cpp and Source/CMakeLists.txt. Don't forget to remove the error throw in inflateCoST.cpp. 

• Finally, you will need to call the inflation method in Source/main.cpp. There should be some examples. 






## The TIF file was created from the CoST using [Fiji](https://imagej.net/software/fiji/), with the SciView plugin installed. In order to create the TIF file...

• Open the CoST in Fiji (ImageJ)  using SciView

• Upload the CoST

• Click on Process > Mesh to Image to generate a TIF file.





## Relevant sections in the PuMA tutorial to run and view are 3D visualization and elasticity. 3d visualization will load the CoST into PuMA so you can see it. However, the elasticity section does not import the TIF; you will have to modfiy the code. I could not get the elasticity section to run properly with the TIF.
